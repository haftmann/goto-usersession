
# Kommandozeile

**query-userconfig** *Anfrage*


# Beschreibung

Mit **query-userconfig** lassen sich Benutzerdaten, wie sie von `goto-usersession`
verwaltet werden, von der Shell aus abfragen.  Dabei hat die *Anfrage* folgendes
Format:

> *Accessor* :== (*Name* | `*`)
>
> *Anfrage* :== *Accessor* (`.` *Accessor*)*

Die Accessoren einer Anfrage werden von links nach rechts abgearbeitet.  Jeder Accessor
operiert auf dem Ergebnis der vorherigen Anfrage; der erste Accessor operiert auf
den gesamten Benutzerdaten.

* Ist der Accessor ein *Name* und besteht das aktuelle Ergebnis aus Dictionaries,
die *Name* als Schlüssel enthalten, sind das folgende Ergebnis die Werte der
betreffenden Einträge in den Dictionaries.

* Ist der Accessor `*` und besteht das aktuelle Ergebnis aus Arrays, sind das folgende
Ergebnis alle Elemente der Arrays.

* Andernfalls ist die Anfrage nicht valide.

Nach Abarbeiten der Anfrage wird das Ergebnis wie folgt ausgegeben:

* Jeder atomare Wert wird auf die Standardausgabe ausgegeben; enthält
ein atomarer String einen Zeilenvorschub, ist die Anfrage nicht valide.

* Für Dictionaries werden ihre Schlüssel auf die Standardausgabe ausgegeben.

* Enthält das Ergebnis ein Array, ist die Anfrage nicht valide.

Die Ausgabe erfolgt jeweils zeilenweise.

Ist eine Anfrage nicht valide, wird mit einem Fehler abgebrochen.


# Weiterlesen

Eine nähere Beschreibung der Benutzerdaten findet sich in der Dokumentation des
Pakets *[goto-usersession](file:///usr/share/doc/goto-usersession/goto-usersession.html)*.
