
'''
    Command line interface.
'''

# Copyright 2011 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


import logging
import sys

from mopynaco import exception
from mopynaco.Sudo_User import Sudo_User

from User_Interface import switch

import goto_usersession
from .Session import Session


def main():

    # initialize basic components
    sudo_user = Sudo_User()
    log = logging.getLogger(Session.application_name)
    log.addHandler(sudo_user.host_user_log_handler(Session.application_name))
    log.setLevel(logging.DEBUG)
    log.info('{} starting with argument(s) {}'.format(Session.application_name, ', '.join(sys.argv[1:])))

    with exception.logging_abort(log.error):

        # argument processing
        session = Session(log, sudo_user)
        parser = exception.Argument_Parser(description = goto_usersession.__doc__.strip())
        subparsers = parser.add_subparsers()
        parser.set_defaults(subcommand = lambda *_: parser.error('No subcommand given.'))
        subparsers.add_parser(session.arg_login).set_defaults(subcommand = session.login)
        subparsers.add_parser(session.arg_logout).set_defaults(subcommand = session.logout)
        subparsers.add_parser(session.arg_refresh_config).set_defaults(subcommand = lambda ui: session.refresh_config())
        args = parser.parse_args()

        # running core application
        switch.run_with(Session.application_name, 'Benutzersitzung', Session.icon_name, args.subcommand)

        # ending core application
        log.info('{} ended'.format(Session.application_name))
