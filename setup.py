
import goto_usersession

from distutils.core import setup

from debian.changelog import Changelog

with open('debian/changelog', 'rb') as reader:
    chlog = Changelog(reader, max_blocks = 1)
version = chlog.get_version().full_version

description = goto_usersession.__doc__.strip()
long_description = '''This package provides a mechanism to inject login and logout plugins
into a user's graphical session.'''


settings = dict(

    name = 'goto-usersession',
    version = version,

    packages = ['goto_usersession'],

    author = 'Florian Haftmann',
    author_email = 'florian.haftmann@muenchen.de',
    description = description,
    long_description = long_description,

    license = 'EUPL',
    url = 'http://www.muenchen.de'

)

setup(**settings)
