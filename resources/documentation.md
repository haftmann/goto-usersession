
# Konzept

*goto-usersession* stellt einen Rahmen für das Management von
interaktiven grafischen Benutzersitzungen bereit.

Dazu gehören:

* Schnittstelle für Plugins zur Beziehung von Konfigurationsdaten (z.\ B.\ aus LDAP)
* Persistenter atomarer Cache für benutzerspezifische Konfigurationsdaten
* Schnittstelle für Plugins zum sequentiellen Lauf bei Login und Logout
* zentrales Logging


# Ablauf einer Benutzersitzung

* Der Benutzer meldet sich an.
* Die Konfigurations-Plugins werden ausgeführt, bei Erfolg werden die lokal gespeicherten
  Konfigurationsdaten upgedated.
* Die Login-Plugins werden hintereinander ausgeführt.
  * Bricht eines davon ab, kommt es zum [Rollback](#sitzungs-plugins) und die Benutzersitzung endet.
  * Ansonsten wird die Benutzersitzung fortgesetzt.
* Die interaktive Desktop-Sitzung des Benutzers startet – der Benutzer ist am Zug.
* Der Benutzer meldet sich ab.
* Die Logout-Plugins werden hintereinander ausgeführt.

Über den ganzen Vorgang wird pro Benutzer ein zentrales [Log](#pfade) geführt.


# Konfigurationsdaten und Konfigurations-Plugins

Konfigurationsdaten werden bezogen über Plugins.  Diese Plugins sind in
*@{identval goto_usersession.Session.Session.dir_obtain}* abgelegt.

Ein Plugin ist ein Python-Snippet mit Endung
*@{identval goto_usersession.Session.Session.suffix_obtain}*.
Darin muss eine Funktion namens
*@{identval goto_usersession.Session.Session.script_obtain_function_name}* vorhanden sein, die
ein Dictionary als Rückgabewert liefert.  Das Dictionary wiederum besteht rekursiv aus
Dictionaries, Listen oder Text (unicode) und repräsentiert so einen abstrakten Datenbaum.
Wird kein Wert zurückgegeben, zeigt dies einen Fehler an.

Zum Beziehen der Konfiguration werden alle Plugins ausgeführt und aus den
Rückgabewerten aller Plugins ein vereinigter abstrakter Datenbaum erstellt.
Diese Konfigurationsdaten werden lokal gespeichert.

Diese Konfigurationsdaten können über eine [Zugriffsschnittstelle](#abfragen-von-benutzerdaten)
abgegriffen werden.

Liefert ein Konfigurations-Plugin einen Fehler, so wird der gesamte Bezug abgebrochen,
und die bislang gespeicherten Konfigurationsdaten verwendet.


# Sitzungs-Plugins

Während des Logins bzw. des Logouts werden sequentiell Plugins abgearbeitet.  Diese sind
ausführbare Dateien in *@{identval goto_usersession.Session.Session.dir_login}* bzw.
*@{identval goto_usersession.Session.Session.dir_logout}*.  Ihre Reihenfolge ist durch
die lexikalische Ordnung ihrer Dateinamen gegeben.

Login-Plugins werden mit dem Argument *@{identval goto_usersession.Session.Session.arg_login}*
aufgerufen.  Ein Login wird abgebrochen, falls ein Login-Plugin mit Exit-Value
*@{identval goto_usersession.Session.Session.exit_value_abort}* endet.
In diesem Fall kommt es zu einem Rollback: die bereits ausgeführten Login-Plugins
werden in umgekehrter Reihenfolge mit Argument *@{identval goto_usersession.Session.Session.arg_rollback}*
ausgeführt, bevor wieder die Anmeldemaske erscheint.

Logout-Plugins werden mit dem Argument *@{identval goto_usersession.Session.Session.arg_logout}*
aufgerufen.

Durch die expliziten Argumente für die Plugins lässt sich das gleiche Executable via Symlink
sowohl als Login- als auch Logout-Plugin verwenden.


# Abfragen von Benutzerdaten

Die Benutzerdaten eines Benutzers lassen sich in Python beziehen via
*@{identname config_data.retrieve_for in goto_usersession}* und
*@{identname config_data.retrieve in goto_usersession}*.
Das Ergebnis ist eine hierarchische Schachtelung
von Listen und Dictionaries, deren Blätterelemente atomare Werte (Strings, Zahlen) sind.


# Pfade

Konfigurations-Plugins

:   *@{identval goto_usersession.Session.Session.dir_obtain}*

Persistenter atomarer Cache für benutzerspezifische Konfigurationsdaten

:   *@{identval goto_usersession.Session.Session.dir_userconfig_base}*

Sitzungs-Plugins während des Logins

:   *@{identval goto_usersession.Session.Session.dir_login}*

Sitzungs-Plugins während das Logouts

:   *@{identval goto_usersession.Session.Session.dir_logout}*

Logging

:   *@{identval mopynaco.Sudo_User.host_user_logbase}/@{identval goto_usersession.Session.Session.application_name}/*Benutzername*.log*


# Diagnostische Aufrufe

Konfigurationsdaten erneuern

:   *goto-usersession @{identval goto_usersession.Session.Session.arg_refresh_config}*

Login stand-alone ablaufen lassen

:   *goto-usersession @{identval goto_usersession.Session.Session.arg_login}*

Logout stand-alone ablaufen lassen

:   *goto-usersession @{identval goto_usersession.Session.Session.arg_logout}*
