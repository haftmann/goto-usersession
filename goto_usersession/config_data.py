
'''
    Access to user configuration data.
'''

# Copyright 2013 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


__all__ = ['retrieve', 'retrieve_for', 'known_users']


from os import path
import json
import argparse
import os

from mopynaco import text
from mopynaco.Sudo_User import Sudo_User

from .Session import Session


def retrieve_for(username):

    loc_userconfig = Session.userconfig_for(username)
    if path.exists(loc_userconfig):
        with text.reading_utf8(loc_userconfig) as reader:
            return json.load(reader)
    else:
        return {}


def retrieve(sudo_user):

    assert isinstance(sudo_user, Sudo_User)

    return retrieve_for(sudo_user.username)


def known_users():

    return os.listdir(Session.dir_userconfig_base) \
      if path.exists(Session.dir_userconfig_base) else []


def query(data, accessor):

    assert isinstance(accessor, str)

    def next_focuses(focus, fragment):

        if isinstance(focus, dict):
            yield focus[fragment]
        elif isinstance(focus, list):
            if fragment == '*':
                for single_focus in focus:
                    yield single_focus
            else:
                yield focus[int(fragment)]
        else:
            raise KeyError('attempt to dive beyond an atomic value: {}'.format(fragment))

    if accessor == '':
        fragments = []
    else:
        fragments = accessor.split('.')
    focuses = [data]

    try:
        for fragment in fragments:
            focuses = [single_focus for focus in focuses
              for single_focus in next_focuses(focus, fragment)]
        for focus in focuses:
            if isinstance(focus, dict):
                for key in list(focus.keys()):
                    yield key
            elif isinstance(focus, list):
                raise ValueError('attempt to retrieve access keys of list')
            else:
                if isinstance(focus, str) and '\n' in focus:
                    raise ValueError('linebreak in retrieved value')
                yield focus
    except KeyError as e:
        raise LookupError('KeyError while looking up {}: {}'.format(accessor, str(e)))
    except ValueError as e:
        raise LookupError('ValueError while looking up {}: {}'.format(accessor, str(e)))


def main():

    sudo_user = Sudo_User()

    parser = argparse.ArgumentParser(description = __doc__.strip())
    parser.add_argument('accessor', metavar = 'ACCESSOR')
    args = parser.parse_args()

    for answer in query(retrieve(sudo_user), args.accessor):
        print(answer)
