
'''
    Session management.
'''

# Copyright 2011 - 2017 Landeshauptstadt München

# Licensed under the EUPL, Version 1.1 or -- as soon they will
# be approved by the European Commission -- subsequent
# versions of the EUPL (the "Licence").  You may not use this
# work except in compliance with the Licence.

# You may obtain a copy of the Licence at:

#   http://www.osor.eu/eupl

# Unless required by applicable law or agreed to in writing,
# software distributed under the Licence is distributed on an
# "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
# either express or implied.

# See the Licence for the specific language governing
# permissions and limitations under the Licence.


from os import path
import json
import sys

from mopynaco.Singleton import Singleton
from mopynaco import dateutil
from mopynaco import pathutil


class Session(Singleton):

    application_name = 'goto-usersession'

    arg_login = 'login'
    arg_logout = 'logout'
    arg_rollback = 'rollback'
    arg_refresh_config = 'refresh_config'

    dir_userconfig_base = '/var/lib/goto-usersession/userconfig'
    name_userconfig = 'userconfig.json'

    dir_obtain = '/etc/goto-usersession/obtain-config.d'
    script_obtain = '/usr/share/goto-usersession/obtain-config'
    script_obtain_function_name = '__call__'
    suffix_obtain = '.snippet.py'

    dir_login = '/etc/goto-usersession/login.d'
    dir_logout = '/etc/goto-usersession/logout.d'

    exit_value_ok = 0
    exit_value_abort = 127

    strict_errors = path.exists('/etc/goto-usersession/strict_errors')

    icon_name = 'limux-tux'
    minimum_width = 650

    def __init__(self, log, sudo_user):

        self.log = log
        self.sudo_user = sudo_user
        self.loc_userconfig = self.userconfig_for(sudo_user.username)


    @classmethod
    def userconfig_for(Class, username):

        return path.join(Class.dir_userconfig_base, username, Class.name_userconfig)


    def abort(self, message):

        self.log.error('aborting: {}'.format(message))
        print('abort: {}'.format(message), file = sys.stderr)
        raise SystemExit(1)


    def refresh_config(self):

        for loc in [path.dirname(self.dir_userconfig_base), self.dir_userconfig_base,
          path.dirname(self.loc_userconfig)]:
            self.sudo_user.ensure_accessible_dir(loc, root_only = True)

        self.log.info('obtaining current user configuration')
        plain_data, _, exit_value = self.sudo_user.capture(self.script_obtain,
          log_verbose = False, log_callback = self.log.debug)
          ## we prefer a separate process space here
        if exit_value != self.exit_value_ok:
            self.log.error('could not obtain current user configuration')
            return

        config = json.loads(plain_data)
        self.log.info('successfully obtained current user configuration')

        with self.sudo_user.as_root():
            with pathutil.atomicity_guard(self.loc_userconfig) as dst:
                with open(dst, 'w') as writer:
                    json.dump(config, writer, allow_nan = False, indent = 2,
                      sort_keys = True)
            self.log.info('written current user configuration')


    def exists_config(self):

        return path.exists(self.loc_userconfig)


    def obtain_last_login(self):

        last = None
        rotate_index = 0
        while last is None:
            suffix = '' if rotate_index == 0 else '.' + str(rotate_index)
            loc_wtmp = '/var/log/wtmp' + suffix
            if not path.exists(loc_wtmp):
                break
            answer, _, _ = self.sudo_user.capture('/usr/bin/last',
              ['-R', '-n2', '-F', '-f', loc_wtmp, self.sudo_user.username])
            lines = answer.split('\n')
            if len(lines) < 4:
                rotate_index += 1
            else:
                last = lines[1]

        if last is None:
            return None

        try:
            return dateutil.parse_unix_date(' '.join(last.split()[3:7]))
        except ValueError:
            return None


    def distill_login_message(self):

        last_login = self.obtain_last_login()
        if last_login is None:
            return 'Sie sind nun zum ersten Mal auf diesem System angemeldet.'
        else:
            return 'Ihre letzte Anmeldung war am {}, den {}. {} {} um {} Uhr'.format(
              last_login.strftime('%A'), last_login.day,
              last_login.strftime('%B'), last_login.year,
              last_login.strftime('%H:%M'))


    def distill_name(self, name):

        return path.basename(name).lstrip('0123456789_').replace('_', ' ')


    def rollback(self, current, plugins_login):

        self.log.info('rolling back login')

        plugins_rollback = reversed(plugins_login[:max(0, current)])
        for plugin in plugins_rollback:
            exit_value = self.sudo_user.call(plugin, [self.arg_rollback], log_callback = self.log.info)
            if exit_value != self.exit_value_ok:
                self.log.error('bad exit value: {}'.format(exit_value))

        self.log.info('end of rollback')


    def login(self, ui):

        self.log.info('begin of login')

        plugins_login = pathutil.get_exec_parts(self.dir_login)
        for plugin in plugins_login:
            self.log.info('login plugin: {}'.format(plugin))

        steps = ['Benutzerkonfiguration…'] + [self.distill_name(plugin) for plugin in plugins_login]

        last_login_message = self.distill_login_message()

        with ui.stepper(steps, subtitle = last_login_message, minimum_width = self.minimum_width) as stepper:

            stepper.announce()
            self.refresh_config()
            if not self.exists_config():
                self.abort('no user configuration')

            for current, plugin in enumerate(plugins_login):
                stepper.announce()
                exit_value = self.sudo_user.call(plugin, [self.arg_login], log_callback = self.log.info)
                if exit_value == self.exit_value_abort:
                    self.rollback(current, plugins_login)
                    self.abort('rollback issued by plugin {}'.format(plugin))
                elif exit_value != self.exit_value_ok:
                    if self.strict_errors:
                        self.rollback(current, plugins_login)
                        self.abort('bad exit value: {}'.format(exit_value))
                    else:
                        self.log.error('bad exit value: {}'.format(exit_value))

        self.log.info('end of login')


    def logout(self, ui):

        self.log.info('begin of logout')

        plugins_logout = pathutil.get_exec_parts(self.dir_logout)
        for plugin in plugins_logout:
            self.log.info('logout plugin: {}'.format(plugin))

        steps = [self.distill_name(plugin) for plugin in plugins_logout] + ['Benutzerkonfiguration…']

        with ui.stepper(steps, minimum_width = self.minimum_width) as stepper:

            for plugin in plugins_logout:
                stepper.announce()
                exit_value = self.sudo_user.call(plugin, [self.arg_logout], log_callback = self.log.info)
                if exit_value != self.exit_value_ok:
                    self.log.error('bad exit value: {}'.format(exit_value))

            stepper.announce()

        self.log.info('end of logout')
